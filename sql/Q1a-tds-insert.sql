--****PLEASE ENTER YOUR DETAILS BELOW****
--Q1a-tds-insert.sql
--Student ID: 30359953
--Student Name:Shenyi Zhang
--Tutorial No: Tutorial 11

SET SERVEROUTPUT ON;


/* Comments for your marker:

love you


*/


/*
1(a) Load selected tables with your own additional test data
*/
-- officer license vin
insert into offence values(001, to_date('2016-1-1 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Clayton VIC 3168', 99, 10000001,'100078','KMHCN45CX9U293237' );
insert into offence values(002, to_date('2016-2-2 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Clayton VIC 3168', 106, 10000006,'100078','KMHCN45CX9U293237' );
insert into offence values(003, to_date('2016-3-2 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Clayton VIC 3168', 110, 10000008,'100078','KMHCN45CX9U293237' );
insert into offence values(004, to_date('2016-4-2 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Clayton VIC 3168', 100, 10000016,'100078','KMHCN45CX9U293237' );
insert into suspension values('100078', to_date('2016-4-2 01:00 AM','YYYY-MM-DD HH:MI AM'), to_date('2019-4-2 01:00 AM','YYYY-MM-DD HH:MI AM'));
insert into offence values(005, to_date('2017-5-2 01:00 AM','YYYY-MM-DD HH:MI AM'), 'NottingHill VIC 3168', 101, 10000020,'100079','2T2BK1BA8BC089027' );
insert into offence values(007, to_date('2017-6-3 01:00 AM','YYYY-MM-DD HH:MI AM'), 'NottingHill VIC 3168', 103, 10000021,'100079','2T2BK1BA8BC089027' );
insert into suspension values('100079', to_date('2017-6-3 01:00 AM','YYYY-MM-DD HH:MI AM'), to_date('2020-6-3 01:00 AM','YYYY-MM-DD HH:MI AM'));
insert into offence values(008, to_date('2017-8-3 01:00 AM','YYYY-MM-DD HH:MI AM'), 'SouthYarra VIC', 100, 10000019,'100080','SALMH134090015425' );
insert into offence values(009, to_date('2017-8-4 01:00 AM','YYYY-MM-DD HH:MI AM'), 'SouthYarra VIC', 100, 10000019,'100080','SALMH134090015425' );
insert into offence values(010, to_date('2017-9-1 01:00 AM','YYYY-MM-DD HH:MI AM'), 'SouthYarra VIC', 102, 10000018,'100080','SALMH134090015425' );
insert into suspension values('100080', to_date('2017-9-1 01:00 AM','YYYY-MM-DD HH:MI AM'), to_date('2020-9-1 01:00 AM','YYYY-MM-DD HH:MI AM'));
insert into offence values(011, to_date('2017-9-30 01:00 AM','YYYY-MM-DD HH:MI AM'), 'BoxHill VIC', 102, 10000015,'100081','2T2BKAB1XFC289347' );
insert into offence values(012, to_date('2018-1-30 01:00 AM','YYYY-MM-DD HH:MI AM'), 'BoxHill VIC', 120, 10000005,'100081','2T2BKAB1XFC289347' );
insert into offence values(013, to_date('2018-2-28 01:00 AM','YYYY-MM-DD HH:MI AM'), 'BoxHill VIC', 102, 10000011,'100082','SALJY1248TA516181' );
insert into offence values(014, to_date('2018-3-16 01:00 AM','YYYY-MM-DD HH:MI AM'), 'GlenWaverly VIC', 102, 10000006,'100083','SALAG2V61EA704128' );
insert into offence values(015, to_date('2018-10-30 01:00 AM','YYYY-MM-DD HH:MI AM'), 'GlenWaverly VIC', 119, 10000009,'100084','JTHGL1EF4F117BL47' );
insert into offence values(016, to_date('2019-5-30 01:00 AM','YYYY-MM-DD HH:MI AM'), 'MountWaverly VIC', 119, 10000003,'100085','KM8JUCBC0A0015290' );
insert into offence values(017, to_date('2019-6-30 01:00 AM','YYYY-MM-DD HH:MI AM'), 'MountWaverly VIC', 119, 10000004,'100086','ZFF74UFA1D0195883' );
insert into offence values(018, to_date('2019-7-01 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Carnegie VIC', 119, 10000020,'100087','WB10172A0YZE32655' );
insert into offence values(019, to_date('2019-7-20 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Carnegie VIC', 119, 10000019,'100089','JTHCF1D29F104CU58' );
insert into offence values(020, to_date('2019-7-25 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Carnegie VIC', 119, 10000017,'100090','SALSK25U57A118844' );
insert into offence (off_no,off_datetime,off_location,dem_code,officer_id,lic_no,veh_vin)
    values(021, to_date('2019-7-25 01:00 AM','YYYY-MM-DD HH:MI AM'), 'Carnegie VIC', 119, 10000011,'100011','1HFSC2213SA700065' );
insert into offence (off_no,off_datetime,off_location,dem_code,officer_id,lic_no,veh_vin)
    values(022, to_date('2019-7-25 01:30 AM','YYYY-MM-DD HH:MI AM'), 'Carnegie VIC', 119, 10000011,'100011','1HFSC2213SA700065' );
commit;

