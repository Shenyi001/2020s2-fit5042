--****PLEASE ENTER YOUR DETAILS BELOW****
--Q3-tds-mods.sql
--Student ID:30359953
--Student Name: Shenyi Zhang
--Tutorial No: Tutorial 11

/* Comments for your marker:

Love you


*/


/*
3(i) Changes to live database 1
how to solve null?
*/


Alter table officer
add(
"Total time booked" numeric(10)  default 0   null 
);

update officer
set "Total time booked" = 
(select nvl(count(o1.off_no),0)  from offence o1 join officer o2 on o1.officer_id = o2.officer_id where officer.officer_id = o2.officer_id group by o1.officer_id);

commit;

/*
3(ii) Changes to live database 2
*/
--PLEASE PLACE REQUIRED SQL STATEMENTS FOR THIS PART HERE
drop table revoke_reason cascade constraints purge;
drop table revoke_record cascade constraints purge;
create table revoke_reason
(
reason_code char(3) not null,
reason varchar(100) not null,
constraint pk_revoke_reson primary key(reason_code) 
);
insert into revoke_reason
values('FOS','First offence exceeding the speed limit by less than 10km/h');
insert into revoke_reason
values('FUE','Faulty equipment used');
insert into revoke_reason
values('DOU','Driver objection upheld');
insert into revoke_reason
values('COH','Court hearing');
insert into revoke_reason
values('EIP','Error in proceedings');
commit;

create table revoke_record
(
revoke_no number(8) not null,
revoked_offence number(8) not null,
revoke_officer number(8) not null,
revoke_date date not null,
revoke_reason not null,
constraint rv_pk primary key(revoke_no),
constraint rv_of_fk foreign key (revoke_officer) references officer(officer_id),
constraint rv_re_kf foreign key (revoke_reason) references revoke_reason(reason_code),
constraint rv_offence_kf foreign key (revoked_offence) references offence(off_no)
);

alter table offence
add(
Revoked char(3) default 'No' not null,
Revoke_no number(8) null,
constraint ck_re check (Revoked in ('Yes','No')),
constraint off_rev_fk foreign key(Revoke_no) references revoke_record(revoke_no)
);


commit;



/*
3(iii) Changes to live database 3
*/
--PLEASE PLACE REQUIRED SQL STATEMENTS FOR THIS PART HERE
drop table color CASCADE constraints purge;
drop table outer_part CASCADE constraints purge;
drop table outer_veh CASCADE constraints purge;
drop sequence sq_cl;
create table color 
(color_no number(3) not null,
color varchar(10) not null,
constraint pk_cl primary key(color_no)
);
create sequence sq_cl;
insert into color(color_no,color)
select sq_cl.nextval, t.veh_maincolor
from
(
(select distinct veh_maincolor from vehicle) t
);

create table outer_part
(
part_code char(2) not null,
part_name varchar(10) not null,
constraint op_pk primary key(part_code)
);

insert into outer_part values ('SP','Spoiler');
insert into outer_part values ('BM','Bumper');
insert into outer_part values ('GR','Grilles');
commit;

create table outer_veh
(
veh_vin char(17) not null,
outer_part_code char(2) not null,
color_no number(3),
constraint ov_pk primary key(veh_vin,outer_part_code),
constraint ov_fk_veh foreign key(veh_vin) references vehicle(veh_vin),
constraint ov_fk_cl foreign key(color_no) references color(color_no)
);




















