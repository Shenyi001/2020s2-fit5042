--****PLEASE ENTER YOUR DETAILS BELOW****
--Q2-tds-queries.sql
--Student ID:30359953
--Student Name:Shenyi Zhang
--Tutorial No: Tutorial 11

/* Comments for your marker:




*/


/*
2(i) Query 1
*/

select dem_points as "Demerit Points", dem_description as "Demerit Description"
from demerit
where dem_description like '%heavy%' or dem_description like '%Heavy%' or dem_description like 'Exceed%'
order by dem_points, dem_description;

/*
2(ii) Query 2
*/

select veh_maincolor as "Main Colour", veh_vin as "VIN", to_char(veh_yrmanuf,'YYYY') as " Year Manufactured"
from vehicle
where veh_modname in ('Range Rover','Range Rover Sport') and extract(year from veh_yrmanuf) between '2012' and '2014'
order by veh_yrmanuf desc, veh_maincolor;


/*
2(iii) Query 3
*/
--PLEASE PLACE REQUIRED SQL STATEMENT FOR THIS PART HERE

select d.lic_no as "License No.", lic_fname || ' ' || lic_lname as "Driver Fullname", to_char(lic_dob) as "DOB", lic_street||' '||lic_town||' '||lic_postcode as "Driver Address",
to_char(sus_date) as "Suspended On", to_char(sus_enddate) as "Suspended Till"
from driver d join suspension s on d.lic_no = s.lic_no
where s.sus_enddate >= add_months(sysdate,-30)
order by d.lic_no, sus_date desc;


/*
2(iv) Query 4
-- not finish
*/

select d.dem_code as "Demerit Code", d.dem_description as "Demerit Description", count(o.off_no)as "Total Offences (All Months)",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 1 and dem_code = d.dem_code
group by dem_code),0) as "Jan",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 2 and dem_code = d.dem_code
group by dem_code),0) as "Feb",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 3 and dem_code = d.dem_code
group by dem_code),0) as "Mar",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 4 and dem_code = d.dem_code
group by dem_code),0) as "Apr",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 5 and dem_code = d.dem_code
group by dem_code),0) as "May",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 6 and dem_code = d.dem_code
group by dem_code),0) as "Jun",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 7 and dem_code = d.dem_code
group by dem_code),0) as "Jul",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 8 and dem_code = d.dem_code
group by dem_code),0) as "Aug",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 9 and dem_code = d.dem_code
group by dem_code),0) as "Sep",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 10 and dem_code = d.dem_code
group by dem_code),0) as "Oct",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 11 and dem_code = d.dem_code
group by dem_code),0) as "Nov",
nvl((select count(*)
from offence 
where extract(month from off_datetime) = 12 and dem_code = d.dem_code
group by dem_code),0) as "Dec"

from demerit d left join offence o on d.dem_code = o.dem_code
group by d.dem_code, d.dem_description
order by count(o.off_no) desc,d.dem_code;
/*
2(v) Query 5
*/

select veh_manufname as "Manufacturer Name",
(select max(count(*))
from (demerit d join offence o on d.dem_code = o.dem_code) join vehicle v on o.veh_vin = v.veh_vin
where d.dem_points >= 2
group by v.veh_manufname) as "Total No. of Offences"
from ((demerit d join offence o on d.dem_code = o.dem_code) join vehicle v on o.veh_vin = v.veh_vin)
where d.dem_points >= 2
group by v.veh_manufname
having count(*) = (select max(count(*))
from (demerit d join offence o on d.dem_code = o.dem_code) join vehicle v on o.veh_vin = v.veh_vin
where d.dem_points >= 2
group by v.veh_manufname)
order by (select max(count(*))
from (demerit d join offence o on d.dem_code = o.dem_code) join vehicle v on o.veh_vin = v.veh_vin
where d.dem_points >= 2
group by v.veh_manufname) desc,  veh_manufname;




/*
2(vi) Query 6
*/


select d.lic_no as "Licence No.", d.lic_fname || ' ' || d.lic_lname as "Driver Name", o.officer_id as "Officer ID", o.officer_fname || ' ' || o.officer_lname as "Officer Name" 
from driver d join officer o on d.lic_lname = o.officer_lname where d.lic_no in (select lic_no from offence 
group by lic_no,dem_code
having count(*)>1)
order by d.lic_no;


/*
2(vii) Query 7
*/



select o1.dem_code as "Demerit Code", d.dem_description as "Demerit Description",o1.lic_no as "Licence No.", d.lic_fname || ' ' || d.lic_lname as "Driver Fullname", count(*) as "Total Times Booked"
from (offence o1 join demerit d on o1.dem_code = d.dem_code) join driver d on d.lic_no = o1.lic_no
group by o1.dem_code, o1.lic_no,d.dem_description,d.lic_fname,d.lic_lname
having count(*) = (select max(count(*)) from offence o2 where o1.dem_code = o2.dem_code group by o2.dem_code, o2.lic_no)
order by o1.dem_code,o1.lic_no;




/*
2(viii) Query 8
*/
/*
select Region , count(*) as "Total Vehicles Manufactured", to_char((100 * (count(*)/(select count(*) from vehicle))),'99.99') ||''|| '%' as "Percentage of Vehicles Manufactured"  
from
(select 
case
when veh_vin between 'A%' and 'C%' then 'Africa'
when veh_vin between 'J%' and 'R%' then 'Asia'
when veh_vin between 'S%' and 'Z%' then 'Europe'
when veh_vin between '1%' and '5%' then 'North America'
when veh_vin between '6%' and '7%' then 'Oceania'
when veh_vin between '8%' and '9%' then 'South America'
else 'Unknown'
end as Region, veh_vin
from vehicle) group by Region
order by count(*), Region;
select lpad('TOTAL',10) as "s",sum(count(*)),sum(100 * (count(*)/(select count(*) from vehicle))) ||''||'%'
from(
select 
case
when veh_vin between 'A%' and 'C%' then 'Africa'
when veh_vin between 'J%' and 'R%' then 'Asia'
when veh_vin between 'S%' and 'Z%' then 'Europe'
when veh_vin between '1%' and '5%' then 'North America'
when veh_vin between '6%' and '7%' then 'Oceania'
when veh_vin between '8%' and '9%' then 'South America'
else 'Unknown'
end as Region, veh_vin
from vehicle)group by Region;
*/





select Region , count(*) as "Total Vehicles Manufactured", lpad((to_char((100 * (count(*)/(select count(*) from vehicle))),'99.99') ||''|| '%'),30) as "Percentage of Vehicles Manufactured"  
from
(select 
case
when substr(veh_vin,1,1) between 'A' and 'C' then 'Africa'
when substr(veh_vin,1,1) between 'J' and 'R' then 'Asia'
when substr(veh_vin,1,1) between 'S' and 'Z' then 'Europe'
when substr(veh_vin,1,1) between '1' and '5' then 'North America'
when substr(veh_vin,1,1) between '6' and '7' then 'Oceania'
when substr(veh_vin,1,1) between '8' and '9' then 'South America'
else 'Unknown'
end as Region, veh_vin
from vehicle) group by Region
union
select lpad('TOTAL',15) as "s",sum(count(*)),lpad((sum(100 * (count(*)/(select count(*) from vehicle))) ||''||'%'),30)
from(
select 
case
when veh_vin between 'A%' and 'C%' then 'Africa'
when veh_vin between 'J%' and 'R%' then 'Asia'
when veh_vin between 'S%' and 'Z%' then 'Europe'
when veh_vin between '1%' and '5%' then 'North America'
when veh_vin between '6%' and '7%' then 'Oceania'
when veh_vin between '8%' and '9%' then 'South America'
else 'Unknown'
end as Region, veh_vin
from vehicle)group by Region
order by "Total Vehicles Manufactured";





