package fit5042.ass.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import fit5042.ass.repository.entities.Contact;
import fit5042.ass.repository.entities.User;

@RequestScoped
@Named(value = "customer")
public class Customer {
	private String industry;	// validation in the range?
	private String postCode;
	private String country;
	private String state;
	private String street;
	private String companyName;
	private int cusId;
	private User managedBy;
	private Set<Contact> contacts;
	
	private String contactPhone;
	private String contactTitle;
	private String contactFirstName;
	private String contactLastName;
	private String contactEmail;
	
	public Customer(String industry, String postCode, String country, String state, String street, String companyName,
			int cusId, User managedBy, Set<Contact> contacts, String contactPhone, String contactTitle,
			String contactFirstName, String contactLastName,String contactEmail) {
		super();
		this.industry = industry;
		this.postCode = postCode;
		this.country = country;
		this.state = state;
		this.street = street;
		this.companyName = companyName;
		this.cusId = cusId;
		this.managedBy = managedBy;
		this.contacts = contacts;
		this.contactPhone = contactPhone;
		this.contactTitle = contactTitle;
		this.contactFirstName = contactFirstName;
		this.contactLastName = contactLastName;
		this.contactEmail = contactEmail;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public Customer() {
		this.contacts = new HashSet<>();
	}
	
	
	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public String getContactFirstName() {
		return contactFirstName;
	}

	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}

	

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCusId() {
		return cusId;
	}

	public void setCusId(int cusId) {
		this.cusId = cusId;
	}

	public User getManagedBy() {
		return managedBy;
	}

	public void setManagedBy(User managedBy) {
		this.managedBy = managedBy;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	
}
