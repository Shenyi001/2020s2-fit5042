package fit5042.ass.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@RequestScoped
@Named("search")
public class Search {

	private Customer customer;
	private int cusId;
	private int conId;
	private String industry;
	private String cusName;
	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public int getConId() {
		return conId;
	}

	public void setConId(int conId) {
		this.conId = conId;
	}

	private CustomerApplication app;
	
	public Search() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "customerApplication");
        app.searchAllCustomer();
    }
	
	public void setApp(CustomerApplication app) {
		this.app = app;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getCusId() {
		return cusId;
	}

	public void setCusId(int cusId) {
		this.cusId = cusId;
	}

	public void searchCustomerById(int cusId) 
    {
       try
       {
            //search this property then refresh the list in PropertyApplication bean
            app.searchCustomerById(cusId);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	public void searchContactById(int conId) 
    {
       try
       {
            //search this property then refresh the list in PropertyApplication bean
            app.searchContactById(conId);
       }
       catch (Exception ex)
       {
           
       }
    }
	
	public void searchCustomerByIndustry(String industry) {
		try {
			app.searchCustomerByIndustry(industry);
		}
		catch(Exception ex) {
			
		}
	}
	
	public void searchCustomerByName(String name) {
		try {
			app.searchCustomerByName(name);
		}
		catch(Exception ex) {
			
		}
	}
	
	public void searchContactByCompany(String name) {
		try {
			app.searchContactByCompany(name);
		}
		catch(Exception ex) {
			
		}
	}
}
