package fit5042.tutex.calculator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateful;

import fit5042.tutex.repository.JPAPropertyRepositoryImpl;
import fit5042.tutex.repository.entities.Property;
/**
* Session Bean implementation class CompareProperty
*/

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	private Set<Property> propRepository;
	
	public ComparePropertySessionBean() {
		this.propRepository = new HashSet<>();
	}
	


	public Set<Property> getPropRepository() {
		return propRepository;
	}



	public void setPropRepository(Set<Property> propRepository) {
		this.propRepository = propRepository;
	}



	public void addProperty(Property property) {
		this.propRepository.add(property);
	}
	
	public void removeProperty(Property property) {
		for (Property p : propRepository) {
			if (p.getPropertyId() == property.getPropertyId()) {
				propRepository.remove(p);
				break;
			}
		}
	}
	
	@Override
	public int bestPerRoom() {
		double bestPrice = 10000000;
		int id = 0;
		double pricePerRoom;
		for (Property p : propRepository) {
			pricePerRoom = p.getPrice() / p.getNumberOfBedrooms();
			if (pricePerRoom <= bestPrice)
			{
				bestPrice = pricePerRoom;
				id = p.getPropertyId();
			}
		}
		return id;
	}
}
