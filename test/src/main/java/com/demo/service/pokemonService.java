package com.demo.service;

import com.demo.model.Pokemon;
import com.demo.model.PokemonRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class pokemonService {
    @Autowired
    PokemonRepo repo;

    public Pokemon get(Long id){
        return repo.findById(id).get();
    }

    public void save(Pokemon pokemon){
        repo.save(pokemon);
    }

    public List<Pokemon> listAll(){
        return (List<Pokemon>) repo.findAll();
    }

    public List<Pokemon> search(String keyword){
        return repo.search(keyword);
    }

    public void delete(Long id) { repo.deleteById(id);}

}
