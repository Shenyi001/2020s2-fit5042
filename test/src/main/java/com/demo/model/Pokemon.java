package com.demo.model;
import javax.persistence.*;

@Entity
public class Pokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pokeId;
    private String pokeName;
    private String pokeType;
    private String pokeSpeed;

    public Pokemon(){

    }

    public long getPokeId() {
        return pokeId;
    }

    public void setPokeId(int pokeId) {
        this.pokeId = pokeId;
    }

    public String getPokeName() {
        return pokeName;
    }

    public void setPokeName(String pokeName) {
        this.pokeName = pokeName;
    }

    public String getPokeType() {
        return pokeType;
    }

    public void setPokeType(String pokeType) {
        this.pokeType = pokeType;
    }

    public String getPokeSpeed() {
        return pokeSpeed;
    }

    public void setPokeSpeed(String pokeSpeed) {
        this.pokeSpeed = pokeSpeed;
    }
}
