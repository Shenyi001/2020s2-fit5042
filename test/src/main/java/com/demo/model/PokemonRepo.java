package com.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PokemonRepo extends JpaRepository<Pokemon, Long>
{
    @Query(
            value = "SELECT p FROM Pokemon p WHERE " +
                    "lower(p.pokeName) LIKE lower(CONCAT('%', :keyword, '%')) OR " +
                    "lower(p.pokeType) LIKE lower(CONCAT('%', :keyword, '%')) OR " +
                    "lower(p.pokeSpeed) LIKE lower(concat('%', :keyword, '%'))"
    )
    public List<Pokemon> search(@Param("keyword") String keyword);
}
