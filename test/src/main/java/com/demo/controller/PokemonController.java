package com.demo.controller;

import com.demo.model.Pokemon;
import com.demo.service.pokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
public class PokemonController {
    @Autowired
    private pokemonService service;

    @RequestMapping(method = RequestMethod.GET,value="/")
    public String printHello(ModelMap model){
        model.addAttribute("message","Hello Spring MVC in maven");
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/home")
    public ModelAndView home1(){
        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject("pageTitle","Home Page");
        modelAndView.addObject("message","Welcome");
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pokemanage")
    public ModelAndView home(){
        List<Pokemon> pokes = service.listAll();
        ModelAndView modelAndView = new ModelAndView("pokeManage");
        modelAndView.addObject("pokes",pokes);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/new")
    public String mapToNew(Map<String,Object> model){
        Pokemon poke = new Pokemon();
        model.put("pokemon",poke);
        return "newPoke";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public String save(@ModelAttribute("pokemon") Pokemon pokemon){
        service.save(pokemon);
        return "redirect:/pokemanage";
    }

    @RequestMapping(method = RequestMethod.GET,value = "/delete")
    public String delete(@RequestParam Long id){
        service.delete(id);
        return "redirect:/pokemanage";
    }

    @RequestMapping(method = RequestMethod.GET,value = "/search")
    public ModelAndView search(@RequestParam String keyword){
        ModelAndView modelAndView = new ModelAndView("search");
        List<Pokemon> pokes = service.search(keyword);
        modelAndView.addObject("pokes",pokes);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/edit")
    public ModelAndView edit(@RequestParam Long id){
        ModelAndView temp = new ModelAndView("edit");
        Pokemon poke = service.get(id);
        temp.addObject("poke",poke);
        return temp;
    }
}
